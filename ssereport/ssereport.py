#!/usr/bin/env python
import sys
import requests, json
import time
import threading
import syslog
import weewx
from weewx.engine import StdService
from weeutil.weeutil import timestamp_to_string, option_as_list

class SSEReport(StdService):
  def __init__(self, engine, config_dict):
    super(SSEReport, self).__init__(engine, config_dict)
    self.endpoint = config_dict['SSEReport']['endpoint']
    self.stationid = config_dict['SSEReport']['stationid']
    self.bind(weewx.NEW_ARCHIVE_RECORD, self.newArchiveRecord) 

  def newArchiveRecord(self, event):
    stationData = event.record.copy()
    stationData['stationId'] = self.stationid
    
    payload = {
      "id": self.stationid,
      "data": "%s" % (json.dumps(stationData))
    }

    headers = {
      'content-type': 'application/json'
    }

    try:
      response = requests.post(self.endpoint, data=json.dumps(payload), headers=headers);
    except:
      pass
