var stationData = {};

// Create the weather station element class.
var WeatherStationElement = React.createClass({
  clickHandler: function() {
    window.location.href= "/" + this.props.stationData.stationId.toLowerCase();
  },

  render: function() {
    var degreesCelsius = ((parseFloat(this.props.stationData.outTemp) - 32.0) * 5.0 / 9.0).toFixed(1);
    var windSpeed = parseFloat(this.props.stationData.windSpeed).toFixed(1); 
   
    return (<div className="weatherStationElement" onClick={this.clickHandler}>
      <li>
        <h3>{ this.props.stationData.stationId }</h3>
        <i className="wi wi-thermometer"><span>{ degreesCelsius }&deg;C</span></i>
        <i className="wi wi-humidity"><span>{ this.props.stationData.outHumidity }%</span></i>
        <i className="wi wi-strong-wind"><span>{ windSpeed } m/s</span></i>
      </li>
    </div>);
  }
});

// Create the weather station list class.
var WeatherStationList = React.createClass({
  render: function() {
    var stationNodes = [];

    // Create an array from our hashmap of weather stations.
    jQuery.each(this.props.data, function (key, obj) {
        stationNodes.push(<WeatherStationElement key={ obj.stationId } stationData={ obj } />);
    });

    return (
      <div className="weatherStationList">
      <h1>V&aelig;rstasjon Drammen-Skisenter</h1>
        <ol>
          { stationNodes }
        </ol>
      </div>
    );
  }
});

// Initial render.
ReactDOM.render(
  <WeatherStationList data={stationData} />,
  document.getElementById('content')
);

// Get data from SSE.
var SSESocket = new EventSource('http://skudsvik.no:8080/aron?getcache=1');
SSESocket.addEventListener('message', function(event) {
  var data;

  try {
    data = JSON.parse(event.data);
  } catch(e) {}
 
  if (data && (typeof data.stationId != 'undefined')) {
    stationData[data.stationId] = data;

    ReactDOM.render(
      <WeatherStationList data={ stationData } />,
      document.getElementById('content')
    );
  }
});
